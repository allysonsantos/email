<?php

/**
 * Arquivo para configuração
 */
// O nome de caixa de correio consiste em um servidor e um caminho de caixa de correio neste servidor
define("CONNECTION", "{imap.gmail.com:993/imap/ssl}");
// Login do e-mail
define("LOGIN", "");
// Senha do e-mail
define("PASSWORD", "");
// Quantidade de mensagens quer obter da caixa de e-mail
define("QUANTITYMESSAGES", 4);

// Pasta com os anexos das mensagens
define("ATTACHMENTSPATH", dirname(dirname(__FILE__)) . '/src/attachments/');

// Filtros para o assunto do e-mail
$list = array(
    "NFSE",
    "NFE",
    "NF",
    "Nota fiscal",
);
define("SUBJECTS", $list);

// URL da api. Exemplo: http://localhost/
define("API", "");
