<?php

class IMAP {

    public $connection;
    public $data;
    public $check;
    public $messageList;

    public function extensionLoaded() {
        if (!extension_loaded('imap')) {
            return false;
        }
        return true;
    }

    public function open() {
        $this->connection = imap_open(CONNECTION, LOGIN, PASSWORD);
        if ($this->connection) {
            return $this->connection;
        }
    }

    public function check() {
        $result = imap_check($this->connection);
        if ($result->Nmsgs > 0) {
            $this->check = $result;
            return true;
        }
    }

    public function fetchOverview() {
        $totalMessages = $this->check->Nmsgs;
        $this->messageList = array_reverse(imap_fetch_overview($this->connection, ($totalMessages - QUANTITYMESSAGES + 1) . ":" . $totalMessages));
    }

    public function parseMessageList() {
        if ($this->messageList) {
            foreach ($this->messageList as $overview) {
                if (in_array($overview->subject, SUBJECTS)) {
                    $num = imap_num_msg($this->connection);
                    if ($num > 0) {
                        $attachment = imap_base64(imap_fetchbody($this->connection, $num, 2));
                        $attachmentName = "NF-" . $overview->uid . ".pdf";
                        $this->attachment($attachment, $attachmentName);

                        $bodyText = imap_fetchbody($this->connection, $num, 1.1);
                        $body = trim(utf8_encode(quoted_printable_decode($bodyText)));
                        $strings = explode("\n", $body);
                        $this->data[] = $this->parseBody($strings, $attachmentName);
                    }
                }
            }
        }
    }

    public function parseBody($strings, $attachmentName) {
        $name = explode(":", $strings[4]);
        $address = explode(":", $strings[5]);
        $value = explode(":", $strings[6]);
        $pay = explode(":", $strings[7]);

        $body = new stdClass();
        $body->name = $name[1];
        $body->address = $address[1];
        $body->value = preg_replace("/[^0-9]/", "", $value[1]);
        $body->pay = $pay[1];
        $body->urlAttachment = ATTACHMENTSPATH . $attachmentName;

        return $body;
    }

    public function attachment($data, $name) {
        $file = fopen(ATTACHMENTSPATH . $name, "w");
        fwrite($file, $data);
        fclose($file);
    }

    public function close() {
        imap_close($this->connection);
    }

}

?>
