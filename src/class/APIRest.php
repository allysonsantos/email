<?php

class APIRest {

    public function post($data) {

        try {
            $context = stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-Type: application/json',
                    'content' => json_encode($data)
                )
            ));

            return file_get_contents(API, false, $context);
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

}
