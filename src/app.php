<?php

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once dirname(dirname(__FILE__)) . '/src/class/APIRest.php';
require_once dirname(dirname(__FILE__)) . '/src/class/IMAP.php';

function imap() {
    $IMAP = new IMAP();
    $IMAP->extensionLoaded();
    $IMAP->open();
    $IMAP->check();
    $IMAP->fetchOverview();
    $IMAP->parseMessageList();
    if ($IMAP->data) {
        return $IMAP->data;
    } else {
        return null;
    }
    $IMAP->close();
}

function apirest($data) {
    $APIRest = new APIRest();
    return $APIRest->post($data);
}

$result = imap();
if ($result) {
    print "<pre>";
    var_dump($result);
    # Chamada da API
    #apirest($result);
} else {
    print "Não contém mensagens!";
}
?>