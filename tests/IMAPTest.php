<?php

require_once dirname(dirname(__FILE__)) . '/config/config.php';
require_once dirname(dirname(__FILE__)) . '/src/class/IMAP.php';

use PHPUnit\Framework\TestCase;

final class IMAPTest extends TestCase {

    public function testExtensionLoaded() {
        $IMAP = new IMAP();
        $this->assertTrue($IMAP->extensionLoaded());
    }

    public function testOpen() {
        $IMAP = new IMAP();
        $this->assertNotEmpty($IMAP->open());
    }

    public function testCheck() {
        $IMAP = new IMAP();
        $IMAP->open();
        $this->assertTrue($IMAP->check());
    }

}
