E-mail
====================

# Tecnologias utilizadas

* PHP - 7.2
* Apache - 2.4

# Estrutura de pastas

* **config** (pasta responsável por definir configurações globais)
* **src**
    * **attachments** (pasta onde são salvos os anexos)
    * **class** (pasta onde ficam as classes)
    * **app.php** (arquivo para executar a integração com a caixa de e-mail)
* **tests** (pasta para definir os testes unitários)


## Executar serviço

### Etapa 1

Foi necessário instalar esses pacotes: `php-imap`, `php-mbstring` e `zip`


### Etapa 2

```
git clone https://allysonsantos@bitbucket.org/allysonsantos/email.git
```
```
cd email
```
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
```
```
php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
```
```
php composer-setup.php
```
```
php -r "unlink('composer-setup.php');"
```
```
./composer.phar install
```

### Etapa 3

Precisa acessar o arquivo `config/config.php` para adicionar o `login` e `senha`. Foi adicionado nesse arquivo filtros para o assunto do e-mail.
Durante o desenvolvimento foi usado o correio eletrônico do `Gmail`.

### Etapa 4

Garantido as etapas 1, 2 e 3. Desta forma, pode usar de 2 formas a integração pela web ou pelo terminal.

**Web**

```
http://localhost/email/src/app.php
```

**Terminal**

```
cd email
```
```
php src/app.php
```

## Teste unitário

Foi realizado um teste unitário de 3 métodos da classe `IMAP.php`. Para utilizar precisa garantir as etapas 1, 2 e 3. Comando para executar:

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/IMAPTest
```